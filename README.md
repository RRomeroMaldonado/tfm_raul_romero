##  TFM_RaulRomero

## _Diseño e implementacion nodo IoT para procesar la señal de un radar: TFM_RaulRomero_

## Descripción
Este repositorio contiene los archivos de Python elaborados para la tarea de clasificación de gestos manuales captados por un radar de tecnología FMCW mediante redes neuronales.
Los gestos con los que se ha trabajado y creado la base de datos son:

| Etiqueta | Gesto |
| ------ | ------ |
| 0 | UpDown |
| 1 | DownUp|
| 2 | Up|
| 3 | Down|



Se tienen dos métodos diferentes de representación de los gestos (espectrogramas y secuencia de imágenes RDM), por lo que hay dos casos distintos:

- Clasificación de espectrogramas

- Clasificación de secuencia imágenes RDM

## Clasificación de Espectrogramas
Para las tareas de clasificación de imágenes se crea el conjunto de datos con los espectrogramas de todas las muestras grabadas (gestos) y la clase de cada gesto.

Para la creación del conjunto de datos con los espectrogramas para el proceso de aprendizaje de la red neuronal se ha creado el script:

```sh
CreateDataset.py
```
La implementación del modelo Deep Convolutional Neural Network (DCNN) se lleva a cabo en el archivo:

```sh
DCNN_model.py
```

Adicionalmente, se ha implementado otra arquitectura de red neuronal llamada AlexNet, dicha implementación se lleva a cabo en el script de Python:

```sh
AlexNet_model.py
```

Una vez acabado el proceso de entrenamiento del modelo DCNN, el script:
```sh
Record_predict_gesture.py
```
realiza las siguientes tareas:

- Captación de la señale del radar del gesto realizado
- Procesado de la señal para obtención del espectrograma asociado al gesto
- Cargar modelo ya entrenado (DCNN o AlexNet)
- Cálculo de la predicción del modelo al espectrograma obtenido

## Clasificación de secuencia imágenes RDM
Para las tareas de clasificación de imágenes RDM se crea el conjunto de datos con la secuecia de imágenes de todas las muestras grabadas (gestos) y la clase de cada gesto.

La creación del conjunto de datos se realiza con el script:
```sh
data_gen.py
```
Para la tarea de clasificación de los gestos representados en secuencias de imágenes RDM se ha creado un modelo de red neuronal convolucional denominado 3DCNN, que puede encontrarse en:
```sh
3DCNN_model.py
```
Finalmente en el script:
```sh
Predict_gestureRDM.py
```
se lleva a cabo lo siguiente:

- Cargar secuencia de imágenes de un gesto 
- Calcular predicción del modelo 3DCNN a dicha secuencia de imágenes
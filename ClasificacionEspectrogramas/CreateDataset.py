# -*- coding: utf-8 -*-
"""
Created on Sun Mar 28 13:38:29 2021

@author: raulr

"""
"""
Archivo para crear el dataset en formato pickle
X.pickle array que contiene las imagenes (100,600,600,3) imagen RGB
y.pickle array que contiene los etiquetas de cada imagen (0-UpDown 1-DownUp 2-Up 3-Down)
"""


import numpy as np
import matplotlib.pyplot as plt
import os
import cv2
import random
import pickle

DATADIR = "C:/Users/raulr/Desktop/Gesture_CNN/Espectrogramas" # Carpeta donde están las subcarpetas de las imagenes de cada gesto
CATEGORIES = ["UpDown", "DownUp", "Up", "Down"]               # Nombre de la carpeta de cada gesto

for category in CATEGORIES:
    path = os.path.join(DATADIR, category)
    for img in os.listdir(path):
        img_array = cv2.imread(os.path.join(path,img), cv2.IMREAD_COLOR)  # Almacenamos en img_array loas matrices de cada imagen
        #plt.imshow(img_array)
        #plt.show()
        break
    break

print(img_array.shape)

#RESHAPE THE IMAGES
IMG_SIZE_X = 600 #600 o 227 para AlexNet
IMG_SIZE_Y = 600
new_array = cv2.resize(img_array, (IMG_SIZE_X,IMG_SIZE_Y))      #Cada imagen es una matriz 600x600x3
plt.imshow(new_array)

#CREATE DATASET
training_data = []

def create_training_data():
    for category in CATEGORIES:
        path = os.path.join(DATADIR, category)
        class_num = CATEGORIES.index(category) # Asociamos las etiquietas 0-UpDown, 1-DownUp, 2-Up, 3-Down
        for img in os.listdir(path):
            try:
                img_array = cv2.imread(os.path.join(path,img), cv2.IMREAD_COLOR)
                new_array = cv2.resize(img_array, (IMG_SIZE_X,IMG_SIZE_Y))
                training_data.append([new_array, class_num])
            except Exception as e:
                pass

create_training_data()

print("Lenght of training dataset is: ")
print(len(training_data))

#Mezclamos las muestras de cada clase
random.shuffle(training_data)

for sample in training_data[:10]:
    print(sample[1])
       
X = []
y = []

for features, label in training_data:
    X.append(features)
    y.append(label)
    
X = np.array(X).reshape(-1, IMG_SIZE_X, IMG_SIZE_Y, 3)  #Almacenamos en X las imágenes RGB 
#Convert list into numpy array
y = np.array(y)

#SAVE THE DATASET 
pickle_out = open("C:/Users/raulr/Desktop/Gesture_CNN/Dataset/Data4Gestures/X_train.pickle", "wb")
pickle.dump(X, pickle_out) # Para guardar X en el archivo X.pickle
pickle_out.close()

pickle_out = open("C:/Users/raulr/Desktop/Gesture_CNN/Dataset/Data6Gestures/y_train.pickle", "wb")
pickle.dump(y, pickle_out)
pickle_out.close()

pickle_in = open("C:/Users/raulr/Desktop/Gesture_CNN/Dataset/Data6Gestures/X_testAlex.pickle", "rb")
X = pickle.load(pickle_in)
print(X[1])

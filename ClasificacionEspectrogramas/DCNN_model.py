# -*- coding: utf-8 -*-
"""
Created on Tue Mar 30 12:20:27 2021

@author: raulr
"""
"""
Implementación del modelo Deep Convolutional Neural Network con la siguiente arquitectura:
        
    CONV + RELU (16 filters (3X3)) + MAXPOOL(2X2) + CONV + RELU(32 filters (3x3))
    + CONV + RELU + MAX-POOL + FLATTEN + FC + SOFTMAZ
   
WARNING: Para ejecutar este archivo, debe hacerse desde el entorno virtual de Python donde se ha
        instalado la librería de Tensorflow

                    -  Para 6 gestos simplemente cambiar los conjuntos de entrenamiento y test -
                    -  y modificar la última capa de salida con 6 nodos                        -
"""

import tensorflow as tf
#from PIL import Image
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D
from tensorflow.keras.preprocessing import image
import pickle
import numpy as np
import matplotlib.pyplot as plt
from mlxtend.plotting import plot_confusion_matrix
from sklearn.metrics import confusion_matrix


#Load training set
X = pickle.load(open("Dataset/Data4Gestures/X_train.pickle", "rb"))  #Poner ruta donde esté almacenado el dataset
y = pickle.load(open("Dataset/Data4Gestures/y_train.pickle", "rb"))

#Load test set
X_test = pickle.load(open("Dataset/Data4Gestures/X_test.pickle", "rb"))
y_test = pickle.load(open("Dataset/Data4Gestures/y_test.pickle", "rb"))

#Print the length of training and test sets
print("Lenght of training set is: ")
print(len(X))
print("Lenght of test set is: ")
print(len(X_test))




#Normalizamos el valor de los datos
X = X/255.0
X_test = X_test/255.0
print("The shape of an image is: ")
print(X.shape[1:])

model = Sequential()

#Stage 1
model.add(Conv2D(16,(3,3), input_shape = X.shape[1:]))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2)))

#Stage 2
model.add(Conv2D(32,(3,3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2)))

#Stage 3
model.add(Conv2D(64,(3,3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2)))

#Stage 4
model.add(Flatten())

#Stage 5
model.add(Dense(256))
model.add(Dropout(0.5)) # Añadimos dropout para evitar overfitting

#Stage 6
model.add(Dense(256))
model.add(Dropout(0.5))

#Stage 7
model.add(Dense(4)) # Si hay 4 gestos ponemos ultima capa de 4
model.add(Activation('softmax')) # Para multiclass se utiliza la funcion de activacion softmax

#Compile the model
model.compile(optimizer='adam', 
              loss='sparse_categorical_crossentropy', # Para multiclass se utiliza categorial_crossentropy
              metrics=['accuracy'])


model.summary()

#Entrenamos el modelo
history = model.fit(X, y,
          batch_size=50,
          epochs = 15,
          validation_split = 0.15) #Validation del 15% de longitud de X, de modo que el 75% será trainning y 15% validation set


# Representacion curva de aprendizaje de función de coste
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train','validation'], loc='upper left')
plt.show()

# Representacion de evolución de accuracy
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('acc')
plt.xlabel('epoch')
plt.legend(['train','validation'], loc='upper left')
plt.show()

#Evaluamos el modelo en el conjunto de prueba
preds = model.evaluate(X_test, y_test)
print ("Loss = " + str(preds[0]))
print ("Test Accuracy = " + str(preds[1]))

#Guardamos el modelo ya entrenado en formato HDF5 para poder trabajar con el posteriormente y hacer predicciones
model.save('DCNN_4Gesture.h5')

"""
CONFUSION MATRIX

"""
y_pred = model.predict_classes(X_test)
mat = confusion_matrix(y_test, y_pred)
plot_confusion_matrix(conf_mat = mat)


# -*- coding: utf-8 -*-
"""
Created on Fri Apr  2 18:16:39 2021

@author: raulr
"""

"""
    Script para la grabación y procesado de la señal del radar para obtener
    el espectrograma del gesto realizado. Se carga el modelo ya entrenado y 
    se realia la predicción al espectrograma obtenido del gesto grabado
    por el radar

"""

import numpy as np
from wrapper.ifxRadarSDK import *
import time
import matplotlib.pyplot as plt
import numpy.fft as fft
from scipy import signal
import cv2
import os
import tensorflow as tf
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D
from tensorflow.keras.preprocessing import image
import pickle


#################################################################################
# Device instantiation and configuration
#################################################################################
device = Device()
frame_format = {"num_samples_per_chirp": 64,
                "num_chirps_per_frame": 16,
                "adc_samplerate_Hz": 2000000,
                "frame_period_us": 100000,
                "lower_frequency_kHz": 59000000,
                "upper_frequency_kHz": 63000000,
                "bgt_tx_power": 31,
                "rx_antenna_mask": 7,
                "tx_mode": 0,
                "chirp_to_chirp_time_100ps": 3915500,
                "if_gain_dB": 33,
                "frame_end_delay_100ps": 400000000,
                "shape_end_delay_100ps": 1500000}
device.set_config(**frame_format)
# dres= c/(2*Bandw)
# Vres = (Vmax *2)/n_chiprs
# Rmax = (Rres * N_samples) / 2

#################################################################################
# Create frame object and get number of rx anthenas
#################################################################################
frame = device.create_frame_from_device_handle()
num_rx = frame.get_num_rx()
Rx1 = []

#################################################################################
# For loop. Get data in each iteration and print it
#################################################################################
for frame_number in range(50):
    try:
        device.get_next_frame(frame)
    except RadarSDKFifoOverflowError:
        print("Fifo Overflow")
        exit(1)

    # Do some processing with the obtained frame.
    # In this example we just dump it into the console
    print("Got frame " + format(frame_number) +
          ", num_antennas={}".format(num_rx))

    mat = frame.get_mat_from_antenna(0) # Nos quedamos solo con la antena Rx1
    print("Antenna", 0, "\n", mat)
    
    #Ponemos los datos en vector columna como antes en el archivo .raw
    Rx1 = np.append(Rx1, mat)
    
fs = 10240

#Procesamos la señal de Rx1 para obtener el espectrograma asociado al gesto grabado
    
plt.specgram(Rx1, NFFT=5120, Fs = fs, noverlap = 1024, cmap = 'winter')
plt.colorbar()
plt.title('Amplitude spectrogram of the signal')
plt.xlabel('Time, s')
plt.ylabel('Frequency, Hz')
#plt.show()

#Guardamos el espectrograma del gesto con nombre: test.png
plt.savefig("C:/Users/raulr/Desktop/Gesture_CNN/testGestures/test.png")
plt.close()


# Cargamos el modelo entrenado en formato HDF5
model = load_model('DCNN_6Gesture.h5')

# Cargamos el espectrograma grabado (test.png) 
img = cv2.imread('C:/Users/raulr/Desktop/Gesture_CNN/testGestures/test.png', cv2.IMREAD_COLOR)

#RESHAPE THE IMAGES
IMG_SIZE_X = 600
IMG_SIZE_Y = 600
img_input = cv2.resize(img, (IMG_SIZE_X,IMG_SIZE_Y))
plt.imshow(img_input)

#Normalizamos el valor de la matriz de la imagen
x = image.img_to_array(img_input)
x = np.expand_dims(x, axis = 0)
x = x/255

# Introducimos la imagen a la red neuronal y se calcula la predicción de la red con la función model.predcit 

val = model.predict(x)    # Segun se creó el dataset 0-UpDown 1-DownUp 2-Up 3-Down
print("class prediction [p(0), p(1), p(2), p(3)] = ")
print(val)

# Calculamos el máximo del vector de predicción para conocer qué etiqueta asocia la red neuronal a la imagen de entrada
CATEGORIES = ["UpDown", "DownUp", "Up", "Down"]
pred_gesture = CATEGORIES[np.argmax(val)]
print("The predicted gesture is: ")
print(pred_gesture)
      

   
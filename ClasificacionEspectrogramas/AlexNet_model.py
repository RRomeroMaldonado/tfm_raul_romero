# -*- coding: utf-8 -*-
"""
Created on Wed Apr 21 19:21:08 2021

@author: raulr
"""

"""
Implementation Deep Convolutional Neural Network AlexNet
        
   
WARNING: Para ejecutar este archivo, debe acerse desde el environment donde se ha
        instalado la librería de tensorflow

"""

import tensorflow as tf
#from tensorflow import keras
#from PIL import Image
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D, BatchNormalization
from tensorflow.keras.preprocessing import image
import pickle
import numpy as np
import matplotlib.pyplot as plt
from mlxtend.plotting import plot_confusion_matrix
from sklearn.metrics import confusion_matrix


#Load training set
X = pickle.load(open("Dataset/Data6Gestures/X_trainAlex.pickle", "rb"))  #Poner ruta donde esté almacenado el dataset de los 6 gestos 227x227x3
y = pickle.load(open("Dataset/Data6Gestures/y_trainAlex.pickle", "rb"))

#Load test set
X_test = pickle.load(open("Dataset/Data6Gestures/X_testAlex.pickle", "rb"))
y_test = pickle.load(open("Dataset/Data6Gestures/y_testAlex.pickle", "rb"))

#Print the length of training and test sets
print("Lenght of training set is: ")
print(len(X))
print("Lenght of test set is: ")
print(len(X_test))



#Normalizamos el valor de X
X = X/255.0
X_test = X_test/255.0
print("The shape of an image is: ")
print(X.shape[1:])

model = Sequential()

# Definimos modelo AlexNet
model.add(Conv2D(96, kernel_size=(11,11), strides=(4,4), activation='relu', input_shape=(300,300,3)))
model.add(BatchNormalization())
model.add(MaxPooling2D(pool_size=(3,3), strides=(2,2)))

model.add(Conv2D(filters=256, kernel_size=(5,5), strides=(1,1), activation='relu', padding="same"))
model.add(BatchNormalization())
model.add(MaxPooling2D(pool_size=(3,3), strides=(2,2)))

model.add(Conv2D(filters=384, kernel_size=(3,3), strides=(1,1), activation='relu', padding="same"))  
model.add(BatchNormalization())
    
model.add(Conv2D(filters=384, kernel_size=(3,3), strides=(1,1), activation='relu', padding="same"))  
model.add(BatchNormalization())

model.add(Conv2D(filters=256, kernel_size=(3,3), strides=(1,1), activation='relu', padding="same"))
model.add(BatchNormalization())
model.add(MaxPooling2D(pool_size=(3,3), strides=(2,2)))

model.add(Flatten())

model.add(Dense(4096, activation = 'relu'))
model.add(Dropout(0.5))

model.add(Dense(4096, activation = 'relu'))
model.add(Dropout(0.5))

model.add(Dense(6,activation='softmax' ))


#Compile the model
model.compile(optimizer = 'adam', 
              loss='sparse_categorical_crossentropy', # Para multiclass se utiliza categorial_crossentropy
              metrics=['accuracy'])

model.summary()

#Entrenamos el modelo
history = model.fit(X, y,
          batch_size=50,
          epochs = 50,
          validation_split = 0.2) #Validation del 20%, de modo que el 80% será trainning y 20% validation set


# Representacion curva de aprendizaje de función de coste
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train','validation'], loc='upper left')
plt.show()

# Representacion de evolución de accuracy
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('acc')
plt.xlabel('epoch')
plt.legend(['train','validation'], loc='upper left')
plt.show()


#Evaluamos el modelo en el conjunto de prueba
preds = model.evaluate(X_test, y_test)
print ("Loss = " + str(preds[0]))
print ("Test Accuracy = " + str(preds[1]))

#Guardamos el modelo ya entrenado
model.save('AlexNet_6Gesture.h5')

"""
CONFUSION MATRIX

"""
y_pred = model.predict_classes(X_test)
mat = confusion_matrix(y_test, y_pred)
plot_confusion_matrix(conf_mat = mat)
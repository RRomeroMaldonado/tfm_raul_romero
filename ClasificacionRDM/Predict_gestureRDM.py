# -*- coding: utf-8 -*-
"""
Created on Wed May  5 10:59:56 2021

@author: raulr
"""

"""
Script de Python para cargar un gesto representado en secuencia de 
50 imágenes RDM, y realizar la predicción con el modelo 3DCNN.
Se graba un nuevo gesto, se realiza el procesmaiento de los datos y las
imágenes se almacenan como una muestra más del conjutno de prueba

"""

import tensorflow as tf
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D
from tensorflow.keras.preprocessing import image
import pickle
import numpy as np
import cv2
import matplotlib.pyplot as plt

from data_gen import ActionDataGenerator

root_data_path='data_files'
data_gen_obj=ActionDataGenerator(root_data_path,temporal_stride=50,temporal_length=50,resize=60)


# Cargamos una muestra (secuencia de 50 imágenes) 
test_data = data_gen_obj.load_samples(data_cat='test')
test_generator = data_gen_obj.data_generator(test_data,batch_size=50,shuffle=False)

for k in range(1):
    x_test,y_test = next(test_generator)
    print ('x shape: ',x_test.shape)
    print ('y shape: ',y_test.shape)
    

x_pred = x_test[28] # Cogemos el sample 28 del conjunto testset, por ejemplo


# Representamos un video (secuencia de 50 imagenes)
num_of_images=50
fig=plt.figure(figsize=(16,16))	
plt.title("one sample with {} frames".format(num_of_images))
subplot_num = int(np.ceil(np.sqrt(num_of_images)))
for i in range(int(num_of_images)):
    ax = fig.add_subplot(subplot_num, subplot_num, i+1)
    ax.imshow(x_pred[i,:,:,::-1])
    plt.xticks([])
    plt.yticks([])
    plt.tight_layout()
plt.show()


x_pred = x_pred.reshape(1,50,60,60,3)

print("The shape of a test sequence (video) is: ")
print(x_pred.shape)


# Cargamos el modelo ya entrenado previamente y guardado en formato HDF5
model = load_model('Modelos/3DCNN_4gestures.h5')

val = model.predict(x_pred)    # Segun se creó el dataset 0-UpDown 1-DownUp 2-Up 3-Down
print("class prediction vector= ")
print(val)


# Obtenemos el máximo del vector de salida con la fución softmax
CATEGORIES = ["UpDown", "DownUp", "Up", "Down"]
pred_gesture = CATEGORIES[np.argmax(val)]
print("The predicted gesture is: ")
print(pred_gesture)
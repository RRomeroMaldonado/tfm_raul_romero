# -*- coding: utf-8 -*-
"""
Created on Wed May 12 19:16:29 2021

@author: raulr
"""


import matplotlib
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import (Activation, Conv3D, Dense, Dropout, Flatten,
                                     MaxPooling3D)

import numpy as np
import pickle

from keras.optimizers import Adam
from keras.utils import np_utils
from keras.utils.vis_utils import plot_model


from mlxtend.plotting import plot_confusion_matrix
from sklearn.metrics import confusion_matrix

#from data_gen import ActionDataGenerator


#Load training set
X = pickle.load(open("Dataset/X_train_video4.pickle", "rb"))  #Poner ruta donde esté almacenado el dataset
y = pickle.load(open("Dataset/y_train_video4.pickle", "rb"))

#Load test set
X_test = pickle.load(open("Dataset/X_test_video4.pickle", "rb"))
y_test = pickle.load(open("Dataset/y_test_video4.pickle", "rb"))

print('num of train_samples: {}'.format(len(X)))
print('num of test_samples: {}'.format(len(X_test)))


def get_model(num_classes=4):
    # Define model
    model = Sequential()
    
    model.add(Conv3D(64, kernel_size=(3, 3, 3), input_shape=(50,60,60,3), padding='same'))
    model.add(Activation('relu'))
    model.add(MaxPooling3D(pool_size=(3, 3, 3), padding='same'))
    
    model.add(Conv3D(32, kernel_size=(3, 3, 3), padding='same'))
    model.add(Activation('relu'))
    model.add(MaxPooling3D(pool_size=(3, 3, 3), padding='same'))
  
    
    model.add(Conv3D(64, kernel_size=(3, 3, 3), padding='same'))
    model.add(Activation('relu'))
    model.add(MaxPooling3D(pool_size=(3, 3, 3), padding='same'))
    model.add(Dropout(0.5))
    
    
    model.add(Flatten())
    model.add(Dense(512, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(4))
    model.add(Activation("softmax"))
    
    
    
    model.compile(loss='sparse_categorical_crossentropy',
                  optimizer='adam', metrics=['accuracy'])
    
    model.summary()
    
    tf.keras.utils.plot_model(model,to_file='model3DCNN.png')
    
    return model

model = get_model()

history = model.fit(X,y,
                    batch_size = 32,
                    epochs=20,
                    validation_split = 0.2)


# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='upper left')
plt.show()

# summarize history for accuracy
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('acc')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='upper left')
plt.show()

preds = model.evaluate(X_test, y_test)
print ("Loss = " + str(preds[0]))
print ("Test Accuracy = " + str(preds[1]))

#Guardamos el modelo ya entrenado
model.save('3DCNN_4gestures.h5')

"""
CONFUSION MATRIX

"""
y_pred = model.predict_classes(X_test)
mat = confusion_matrix(y_test, y_pred)
plot_confusion_matrix(conf_mat = mat)